### This repository contains the ISrc-LEBT PV list ###

* There is one text file per each IOC (except NPM that motion and Image acquistion IOC's are merged together) based on the Device/IOC name in Naming Service.
* 'X' in file name matches any character for example "XXXX-010-PBI-BCM-001" IOC contains both ISrc and LEBT BCM PV's.
